
Vue.component('custom-input', {
    props: ['value', 'question'],
    //template: `<div><label :for="question.name">{{question.label}}</label><input :name="question.name" :type="question.type" v-bind:value="value" v-on:input="$emit('input', $event.target.value)"></div>`,
    created: function () {
        this.$options.template = '<div>' //ouverture div
        switch (this.question.tag) {
            case 'input':
                this.$options.template += `<label :for="question.name">{{question.label}}</label><input :name="question.name" :type="question.type" :placeholder="question.placeholder" :required="question.required" :pattern="question.pattern" v-bind:value="value" v-on:input="$emit('input', $event.target.value)">`;
                break;
            // PREVOIR D'AUTRES CAS DE BALISES (select, texarea, ....)
            default:
                break;
        }
        this.$options.template += '</div>';//fermeture div
    }
})

Vue.component('list-contacts', {
    props: ['data'],
    methods: {},
    template: '<div><ul><li class="enteteContacts">- {{ data.nom }}{{ data.prenom }}</li><li class="separContacts"> {{data.port}}</li></li></ul></div>'
})

var nv = new Vue({
    el: '#zoneGlobale',
    data: {
        selectChoice: '',
        tableauContact: [],
        questionForm: [],
        values: {}
    },
    methods: {
        enregContact: function () {
            // alert(this.values.nom+' / '+this.values.prenom+' / '+this.values.port)
            var STORAGE_KEY = 'MonStockageLocal';
            var todos = JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]');
            todos.push(this.values);
            console.log(this.values);
            localStorage.setItem(STORAGE_KEY, JSON.stringify(todos))
        }
    },
    created: function () {
        this.questionForm.push(
            { id: 1, tag: "input", type: "text", label: "Nom", name: "nom", click: "", idTag: '', class: "", placeholder: "Saisisr un nom", required: 'true', pattern: '^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$' },
            { id: 2, tag: "input", type: "text", label: "Prénom", name: "prenom", click: "", idTag: '', class: "", placeholder: "Saisisr un prénom", required: 'true', pattern: '^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$' },
            { id: 3, tag: "input", type: "tel", label: "Portable", name: "port", click: "", idTag: '', class: "", placeholder: "Saisir un n° de portable", required: 'true', pattern: '^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$' }//format french !!
        );

        var STORAGE_KEY = 'MonStockageLocal';
        var todos = JSON.parse(localStorage.getItem(STORAGE_KEY) || '[]');
        this.tableauContact = todos
        todos.forEach(function (element) {
            //              console.log(element.nom+ ' / ' +element.prenom+ ' / ' +element.port)
            console.log(element)
            
        });
    },

})

